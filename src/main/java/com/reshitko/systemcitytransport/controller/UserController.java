package com.reshitko.systemcitytransport.controller;

import com.reshitko.systemcitytransport.controller.utils.SecurityContext;
import com.reshitko.systemcitytransport.model.user.UserDto;
import com.reshitko.systemcitytransport.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/{login}")
    public UserDto getUserByLogin(@PathVariable("login") String login) {
        UserDto user = new UserDto(userService.getUserByLogin(login));
        return user;
    }

    @GetMapping("/current")
    public UserDto getCurrentUser()
    {
        return new UserDto(userService.getUserByLogin(SecurityContext.get().getLogin()));
    }

}
