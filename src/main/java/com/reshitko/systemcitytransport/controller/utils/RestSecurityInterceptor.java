package com.reshitko.systemcitytransport.controller.utils;

import com.reshitko.systemcitytransport.model.user.UserType;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class RestSecurityInterceptor extends HandlerInterceptorAdapter {

    @Value("${jwt.secret}")
    private String secret;

    @Override
    public boolean preHandle(
            HttpServletRequest request
            , HttpServletResponse response
            , Object handler) throws Exception {

        if("OPTIONS".equals(request.getMethod())) {
            System.out.println("OPTIONS");
            return true;
        }
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if(authHeader == null) {
            System.out.print("auth header null");
        }
        String jwt = authHeader.substring(7);//.split("\\s+")[1];
        //System.out.println(jwt);
        try {
            Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(jwt).getBody();
            //System.out.println(claims);
            CallContext context = new CallContext(claims.getSubject()
                    , claims.get("user-login", String.class)
                    , claims.get("role", String.class));
            //System.out.println(context);
            SecurityContext.set(context);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
}
