package com.reshitko.systemcitytransport.controller.utils;

import com.reshitko.systemcitytransport.model.user.UserType;

public class CallContext {
    private String subject;
    private String login;
    private UserType role;

    @Override
    public String toString() {
        return "CallContext{" +
                "subject='" + subject + '\'' +
                ", login='" + login + '\'' +
                ", role=" + role +
                '}';
    }

    public CallContext(String subject, String login, String role) {
        this.subject = subject;
        this.login = login;
        this.role = UserType.valueOf(role);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }
}
