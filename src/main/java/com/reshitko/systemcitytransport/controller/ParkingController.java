package com.reshitko.systemcitytransport.controller;

import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.parking.ParkingDto;
import com.reshitko.systemcitytransport.model.parking.front.ParkingFrontDto;
import com.reshitko.systemcitytransport.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/parkings")
public class ParkingController {

    @Autowired
    private ParkingService parkingService;

    @GetMapping("/find")
    public List<ParkingFrontDto> findParkings() {
        List<Parking> parkings = parkingService.findParkings();
        List<ParkingFrontDto> result = new ArrayList<>();
        for(var x: parkings) {
            result.add(new ParkingFrontDto((x)));
        }
        return result;
    }

    @PostMapping
    public ParkingFrontDto createParking(@RequestBody ParkingFrontDto parkingDto) {
        Parking p = parkingService.createParking(
                new Parking(parkingDto));
        return new ParkingFrontDto(p);
    }
//    @PostMapping
//    public ParkingDto createParking(@RequestBody ParkingDto parkingDto) {
//        Parking p = parkingService.createParking(
//                new Parking(parkingDto));
//        return new ParkingDto(p);
//    }

    @PutMapping
    public void alterParking(@RequestBody ParkingDto parkingDto) {
        parkingService.alterParking(new Parking(parkingDto));
    }

    @DeleteMapping("/{id}")
    public void deleteParking(@PathVariable("id") long id) {
        parkingService.deleteParking(id);
    }
}
