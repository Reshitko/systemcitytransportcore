package com.reshitko.systemcitytransport.controller;

import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.vehicle.*;
import com.reshitko.systemcitytransport.model.vehicle.front.BycycleFrontDto;
import com.reshitko.systemcitytransport.model.vehicle.front.ScooterFrontDto;
import com.reshitko.systemcitytransport.model.vehicle.front.VehicleFrontDto;
import com.reshitko.systemcitytransport.model.vehicle.front.VehicleFrontGeoPosition;
import com.reshitko.systemcitytransport.service.ParkingService;
import com.reshitko.systemcitytransport.service.VehicleService;
import com.reshitko.systemcitytransport.service.VehicleTelemetryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private ParkingService parkingService;

    @Autowired
    private VehicleTelemetryServiceImpl vehicleTelemetryService;
    @GetMapping
    public List<VehicleFrontDto> findAll() {
        List<Vehicle> vehicles = vehicleService.findVehicles();
        List<VehicleFrontDto> result = new ArrayList<>();
        for (var v: vehicles) {
            Parking p = parkingService.findParking(v);
            VehicleFrontDto dto = new VehicleFrontDto(v, p);
            var tel = vehicleTelemetryService.getTelemetry(v);
            if(tel.size() > 0) {
                dto.setGeoPosition(new VehicleFrontGeoPosition(tel.get(0).getLatitude(), tel.get(0).getLongitude()));
            }
            result.add(dto);
        }
        return result;
    }
    @GetMapping("/find")
    public List<VehicleDto> findVehicles(@RequestParam(name = "type", required = false)  VehicleType type,
                                         @RequestParam(name = "occupied", required = false, defaultValue = "False") Boolean occupied) {
        List<Vehicle> vehicles = vehicleService.findByFilter(type, occupied);
        List<VehicleDto> result = new ArrayList<>();
        for (var v: vehicles) {
            result.add(new VehicleDto(v));
        }
        return result;
    }

    @PostMapping
    public VehicleFrontDto createVehicle(@RequestBody VehicleFrontDto vehicleDto) {
        Long parkingId = Long.parseLong(vehicleDto.getParking().getId());

        if (vehicleDto instanceof ScooterFrontDto) {
            return new ScooterFrontDto((Scooter) vehicleService
                    .createVehicle(new Scooter((ScooterFrontDto)vehicleDto), parkingId));
        } else if (vehicleDto instanceof  BycycleFrontDto) {
            return new BycycleFrontDto((Bycycle)vehicleService
                    .createVehicle(new Bycycle(
                            (BycycleFrontDto)vehicleDto), parkingId));
        }
        else {
            throw new IllegalArgumentException();
        }
    }

    @PostMapping("/telemetry")
    public void addTelemetry(@RequestBody VehicleTelemetry vehicleTelemetry) {
        vehicleTelemetryService.addTelemetry(vehicleTelemetry);
    }
//    @PostMapping
//    public VehicleDto createVehicle(@RequestBody VehicleDto vehicleDto) {
//        if (vehicleDto instanceof ScooterDto) {
//            return new ScooterDto((Scooter) vehicleService.createVehicle(new Scooter((ScooterDto)vehicleDto)));
//        } else if (vehicleDto instanceof  BycycleDto) {
//            return new BycycleDto((Bycycle)vehicleService.createVehicle(new Bycycle((BycycleDto)vehicleDto)));
//        }
//        else {
//            throw new IllegalArgumentException();
//        }
//    }

    @DeleteMapping("/{id}")
    public void deleteVehicle(@PathVariable("id") String id) {
        vehicleService.deleteVehicle(id);
    }
}
