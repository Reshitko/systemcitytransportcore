package com.reshitko.systemcitytransport.controller;

import com.reshitko.systemcitytransport.controller.utils.SecurityContext;
import com.reshitko.systemcitytransport.model.rent.Rent;
import com.reshitko.systemcitytransport.model.rent.RentDto;
import com.reshitko.systemcitytransport.model.rent.RentFrontDto;
import com.reshitko.systemcitytransport.model.vehicle.VehicleType;
import com.reshitko.systemcitytransport.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/trip")
public class TripController {

    @Autowired
    private TripService tripService;

    @PostMapping
    public synchronized Rent createTrip(@RequestParam(name = "vehicleSerial") String vehicleSerial) {
        Rent r = tripService.createTrip(vehicleSerial, SecurityContext.get().getLogin());
        return r;
    }

    @PutMapping
    public void finishTrip(@RequestParam(name = "rentId") long rentId, @RequestParam(name = "parkingId") long parkingId) {
        tripService.finishTrip(rentId, parkingId);
    }

    @PostMapping("/view")
    public List<RentFrontDto> findRents() {
        List<Rent> rents = tripService.findRents();
        List<RentFrontDto> resultRents = new ArrayList<>();
        for(var r : rents) {
            resultRents.add(new RentFrontDto(r));
        }
        return resultRents;
    }
}
