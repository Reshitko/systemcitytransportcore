package com.reshitko.systemcitytransport.access;

public interface ParkingDao<Parking> {
    Parking save(Parking parking);
}
