package com.reshitko.systemcitytransport.access;

import com.reshitko.systemcitytransport.model.user.User;

import java.util.Optional;

public interface UserDao {
    public Optional<User> getUserByLogin(String login);
}
