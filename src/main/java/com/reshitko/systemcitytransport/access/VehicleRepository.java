package com.reshitko.systemcitytransport.access;

import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository  extends JpaRepository<Vehicle, String> {
    List<Vehicle> findByIsOccupiedFalse();
    List<Vehicle> findByType(VehicleType type);
}
