package com.reshitko.systemcitytransport.access;

import com.reshitko.systemcitytransport.model.parking.Parking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ParkingRepository extends JpaRepository<Parking, Long>{//, ParkingDao<Parking> {
}
