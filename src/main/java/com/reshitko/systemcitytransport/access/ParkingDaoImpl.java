package com.reshitko.systemcitytransport.access;

import com.reshitko.systemcitytransport.model.parking.Parking;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class ParkingDaoImpl implements ParkingDao<Parking> {
    @PersistenceContext
    private EntityManager em;
    @Override
    public Parking save(Parking parking) {
        String sql = "INSERT INTO PARKING VALUES(null,?,?,?)";// RETURNING ID
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, parking.getLattitude());
        q.setParameter(2, parking.getLongtitude());
        q.setParameter(3, parking.getRadius());
        q.executeUpdate();
        System.out.print((long)q.getSingleResult());
        return null;
    }
}
