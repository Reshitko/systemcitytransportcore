package com.reshitko.systemcitytransport.access;

import com.reshitko.systemcitytransport.model.rent.Rent;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TripRepository extends JpaRepository<Rent, Long> {
    long countRentByDateToAndVehicle(LocalDateTime t, Vehicle v);

    List<Rent> findByIdAndParkingTo(long id, long parkingId);

    Optional<Rent> findFirstByVehicleOrderByDateFromDesc(Vehicle vehicle);

    List<Rent> findTop25ByOrderByDateFromDesc();
}
