package com.reshitko.systemcitytransport.access;

import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleTelemetry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface VehicleTelemetryRepository extends JpaRepository<VehicleTelemetry, String> {
    List<VehicleTelemetry> findBySerialNumberOrderByDatetimeDesc(String serial);
}
