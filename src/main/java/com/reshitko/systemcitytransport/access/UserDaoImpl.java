package com.reshitko.systemcitytransport.access;

import com.reshitko.systemcitytransport.model.user.User;
import com.reshitko.systemcitytransport.model.user.UserType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

@Component
public class UserDaoImpl implements UserDao {
    @Override
    public Optional<User> getUserByLogin(String login) {
        return Optional.of(new User(login, "password", UserType.ADMIN, new BigDecimal(0)));
    }
}
