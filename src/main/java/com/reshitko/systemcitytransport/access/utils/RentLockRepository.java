package com.reshitko.systemcitytransport.access.utils;

import com.reshitko.systemcitytransport.model.utils.RentLock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentLockRepository extends JpaRepository<RentLock, String> {
}
