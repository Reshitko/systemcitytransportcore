package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.model.user.User;

import java.util.Optional;

public interface UserService {

    User getUserByLogin (String login);
}
