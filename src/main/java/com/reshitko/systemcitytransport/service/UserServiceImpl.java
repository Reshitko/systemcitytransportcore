package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.access.UserDao;
import com.reshitko.systemcitytransport.access.UserRepository;
import com.reshitko.systemcitytransport.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserServiceImpl implements UserService {

//    @Autowired
//    private UserDao userDao;
    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserByLogin(String login) {
        //User user = userDao.getUserByLogin(login).orElseThrow(IllegalArgumentException::new);
        User user = userRepository.findById(login).orElseThrow(IllegalArgumentException::new);
        return user;
    }
}
