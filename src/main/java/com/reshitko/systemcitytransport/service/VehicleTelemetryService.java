package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleTelemetry;

import java.util.List;

public interface VehicleTelemetryService {
    public List<VehicleTelemetry> getTelemetry(Vehicle vehicle);
    public void addTelemetry(VehicleTelemetry vt);
}
