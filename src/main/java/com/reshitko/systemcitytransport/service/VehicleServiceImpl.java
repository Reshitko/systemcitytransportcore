package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.access.ParkingRepository;
import com.reshitko.systemcitytransport.access.TripRepository;
import com.reshitko.systemcitytransport.access.UserRepository;
import com.reshitko.systemcitytransport.access.VehicleRepository;
import com.reshitko.systemcitytransport.access.utils.RentLockRepository;
import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.rent.Rent;
import com.reshitko.systemcitytransport.model.user.User;
import com.reshitko.systemcitytransport.model.utils.RentLock;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class VehicleServiceImpl implements VehicleService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ParkingRepository parkingRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private RentLockRepository rentLockRepository;

    @Autowired
    private TripRepository tripRepository;

    @Override
    @Transactional
    public Vehicle createVehicle(Vehicle vehicle, Long parkingId) {
        if (vehicle.getSerialNumber() == null) {
            var vv = vehicleRepository.findByType(vehicle.getType());
            vehicle.setSerialNumber(String.format("%s-%d",vehicle.getType().name(),vv.size()+1));
        }
        Vehicle v = vehicleRepository.save(vehicle);
        RentLock rL = new RentLock(vehicle.getSerialNumber());
        rentLockRepository.save(rL);
        User u = userRepository.findById("ADMIN").orElseThrow();
        Parking p = parkingRepository.findById(parkingId).orElseThrow();
        Rent r = new Rent(null, LocalDateTime.now(), p, p, v, u);
        r.setDateTo(LocalDateTime.now());
        //TODO fix generated id setting
        r.setId(0L);
        tripRepository.save(r);
        return v;
    }

    @Override
    public List<Vehicle> findVehicles() {
    return vehicleRepository.findAll();//findByIsOccupiedFalse();//findByIsOccupiedFalse123();
    }

    @Override
    public List<Vehicle> findByFilter(VehicleType type, Boolean isOccupied) {
        Vehicle v = new Vehicle();
        v.setType(type);
        v.setOccupied(isOccupied);
        Example<Vehicle> example = Example.of(v, ExampleMatcher.matchingAll());
        return vehicleRepository.findAll(example);
    }

    @Override
    @Transactional
    public void deleteVehicle(String id) {
        vehicleRepository.deleteById(id);
        rentLockRepository.deleteById(id);
    }
}
