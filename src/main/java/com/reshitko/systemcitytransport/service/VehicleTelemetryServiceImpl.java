package com.reshitko.systemcitytransport.service;


import com.reshitko.systemcitytransport.access.VehicleRepository;
import com.reshitko.systemcitytransport.access.VehicleTelemetryRepository;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleTelemetry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class VehicleTelemetryServiceImpl implements VehicleTelemetryService {
    @Autowired
    private VehicleTelemetryRepository vehicleTelemetryRepository;

    public List<VehicleTelemetry> getTelemetry(Vehicle vehicle){
        return vehicleTelemetryRepository.findBySerialNumberOrderByDatetimeDesc(vehicle.getSerialNumber());
    }

    @Override
    public void addTelemetry(VehicleTelemetry vehicleTelemetry) {
        vehicleTelemetry.setDatetime(LocalDateTime.now());
        vehicleTelemetryRepository.save(vehicleTelemetry);
    }
}
