package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.access.ParkingDao;
import com.reshitko.systemcitytransport.access.ParkingRepository;
import com.reshitko.systemcitytransport.access.TripRepository;
import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.parking.ParkingDto;
import com.reshitko.systemcitytransport.model.rent.Rent;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class ParkingServiceImpl implements ParkingService {

    @Autowired
    private ParkingRepository parkingRepository;

    @Autowired
    private TripRepository tripRepository;
    @Override
    public List<Parking> findParkings() {
        return parkingRepository.findAll();
    }

    @Override
    public Parking createParking(Parking parking) {
        //parking.clearVehicleTypes();
        Parking res = parkingRepository.save(parking);
        //Parking res = parkingRepository.saveAndFlush(parking);
        return res;
    }

    @Override
    public void alterParking(Parking parking) {
        //javax.persistence.EntityNotFoundException: Unable to find com.reshitko.systemcitytransport.model.parking.Parking with id 67300
        Parking parkingToUpdate = parkingRepository.getById(parking.getId());
        parkingToUpdate.setLattitude(parking.getLattitude());
        parkingToUpdate.setLongtitude(parking.getLongtitude());
        parkingToUpdate.setRadius(parking.getRadius());
        parkingToUpdate.setVehicleTypes(parking.getVehicleTypes());
        parkingRepository.save(parkingToUpdate);
    }

    @Override
    public void deleteParking(long id) {
        //org.springframework.dao.EmptyResultDataAccessException: No class com.reshitko.systemcitytransport.model.parking.Parking entity with id 673 exists!
        parkingRepository.deleteById(id);
    }

    @Override
    public Parking findParking(Vehicle vehicle) {
        Rent r = tripRepository.findFirstByVehicleOrderByDateFromDesc(vehicle).orElseThrow();
        if(r.getParkingTo() == null) {
            return r.getParkingFrom();
        } else {
            return r.getParkingTo();
        }
    }
}
