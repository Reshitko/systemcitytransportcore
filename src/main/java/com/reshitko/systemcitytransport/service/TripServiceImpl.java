package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.access.ParkingRepository;
import com.reshitko.systemcitytransport.access.TripRepository;
import com.reshitko.systemcitytransport.access.UserRepository;
import com.reshitko.systemcitytransport.access.VehicleRepository;
import com.reshitko.systemcitytransport.access.utils.RentLockRepository;
import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.rent.Rent;
import com.reshitko.systemcitytransport.model.user.User;
import com.reshitko.systemcitytransport.model.utils.RentLock;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleTelemetry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.*;

@Component
public class TripServiceImpl implements TripService {

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ParkingRepository parkingRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private RentLockRepository rentLockRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private VehicleTelemetryService vehicleTelemetryService;

    @Override
    public Rent createTrip(Rent rent) {
        return null;
    }

    @Override
    @Transactional//(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)//(propagation = Propagation.REQUIRES_NEW)
    public Rent createTrip(String vehicleSerial, String userName) {

//        Map<String, Object> properties = new HashMap<>();
//        properties.put("javax.persistence.lock.timeout", 0L);
//
//        RentLock rentLock = entityManager.find(RentLock.class,vehicleSerial, LockModeType.PESSIMISTIC_WRITE, properties);
        //entityManager.lock(rentLock, LockModeType.PESSIMISTIC_WRITE, properties);

        Vehicle v = vehicleRepository.findById(vehicleSerial).orElseThrow();
//
//        Parking p = parkingRepository.findById(1L).orElseThrow();

        User u = userRepository.findById(userName).orElseThrow();

        Rent r = tripRepository.findFirstByVehicleOrderByDateFromDesc(v).orElseThrow();
        if(r.getDateTo() == null) {
            throw new RuntimeException("rent already exists");
        }
        Rent rent = new Rent(
                null,
                LocalDateTime.now(),
                null,
                r.getParkingTo(),
                r.getVehicle(),
                u);
        //TODO fix generate id setting
        rent.setId(0L);
//        Query q = entityManager.createQuery("SELECT r FROM Rent r WHERE r.dateTo = ?1 and r.vehicle = ?2");
//        q.setParameter(1, null);
//        q.setParameter(2, v);
//        var res = q.getResultList();

//        var res = tripRepository.countRentByDateToAndVehicle(null, v);
//        if(res > 0) {
//            throw new RuntimeException("rent already created");
//        }

        Rent rent2 = tripRepository.save(rent);
        return rent2;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void finishTrip(long rentId, long parkingId) {
        Rent rent = tripRepository.findById(rentId).orElseThrow();
        Parking p = parkingRepository.findById(parkingId).orElseThrow();
        rent.setDateTo(LocalDateTime.now());
        rent.setParkingTo(p);
        VehicleTelemetry tel = new VehicleTelemetry();
        tel.setDatetime(rent.getDateTo());
        tel.setSerialNumber(rent.getVehicle().getSerialNumber());
        tel.setLatitude(p.getLattitude());
        tel.setLongitude(p.getLongtitude());
        vehicleTelemetryService.addTelemetry(tel);
    }

    @Override
    public List<Rent> findRents() {
        return tripRepository.findTop25ByOrderByDateFromDesc();
    }


}
