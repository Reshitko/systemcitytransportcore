package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.rent.Rent;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;

import java.util.List;

public interface TripService {
    Rent createTrip(Rent rent);

    Rent createTrip(String vehicleSerial, String userName);

    void finishTrip(long rentId, long parkingId);

    public List<Rent> findRents();
}
