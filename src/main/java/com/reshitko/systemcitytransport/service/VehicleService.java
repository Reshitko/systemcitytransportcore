package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleType;

import java.util.List;

public interface VehicleService {
    Vehicle createVehicle(Vehicle vehicle, Long parkingId);
    List<Vehicle> findVehicles();
    List<Vehicle> findByFilter(VehicleType type, Boolean isOccupied);

    void deleteVehicle(String id);
}
