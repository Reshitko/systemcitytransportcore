package com.reshitko.systemcitytransport.service;

import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.parking.ParkingDto;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;

import java.util.List;
import java.util.UUID;

public interface ParkingService {
    public List<Parking> findParkings();
    public Parking createParking(Parking parking);
    public void alterParking(Parking parking);
    public void deleteParking(long id);
    public Parking findParking(Vehicle vehicle);
}
