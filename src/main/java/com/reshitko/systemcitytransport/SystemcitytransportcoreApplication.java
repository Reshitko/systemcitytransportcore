package com.reshitko.systemcitytransport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@PropertySource("classpath:application.yml")
@EnableJpaRepositories
@EnableTransactionManagement
public class SystemcitytransportcoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemcitytransportcoreApplication.class, args);
	}

}
