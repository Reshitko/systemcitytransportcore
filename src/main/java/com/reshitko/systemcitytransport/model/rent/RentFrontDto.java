package com.reshitko.systemcitytransport.model.rent;

import com.reshitko.systemcitytransport.model.parking.front.ParkingFrontDto;
import com.reshitko.systemcitytransport.model.user.UserDto;
import com.reshitko.systemcitytransport.model.vehicle.front.VehicleFrontDto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class RentFrontDto {
    private Long id;

    private String status;
    private UserDto user;
    private VehicleFrontDto vehicle;
    private LocalDateTime startDateTime;
    private LocalDateTime finishDateTime;
    private ParkingFrontDto startParking;
    private ParkingFrontDto finishParking;
    private BigDecimal totalPrice;

    public RentFrontDto(){}

    public RentFrontDto(Rent rent){
        this.id = rent.getId();
        this.status = rent.getDateTo()!=null ? "FINISHED" : "ONGOING";
        this.user = new UserDto(rent.getUser());
        this.vehicle = new VehicleFrontDto(rent.getVehicle());
        this.startDateTime = rent.getDateFrom();
        this.finishDateTime = rent.getDateTo();
        this.startParking = new ParkingFrontDto(rent.getParkingFrom());
        this.finishParking = rent.getParkingTo()!=null ? new ParkingFrontDto(rent.getParkingTo()) : null;
        this.totalPrice = new BigDecimal(0);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public VehicleFrontDto getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleFrontDto vehicle) {
        this.vehicle = vehicle;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getFinishDateTime() {
        return finishDateTime;
    }

    public void setFinishDateTime(LocalDateTime finishDateTime) {
        this.finishDateTime = finishDateTime;
    }

    public ParkingFrontDto getStartParking() {
        return startParking;
    }

    public void setStartParking(ParkingFrontDto startParking) {
        this.startParking = startParking;
    }

    public ParkingFrontDto getFinishParking() {
        return finishParking;
    }

    public void setFinishParking(ParkingFrontDto finishParking) {
        this.finishParking = finishParking;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
