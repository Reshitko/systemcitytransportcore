package com.reshitko.systemcitytransport.model.rent;

import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.user.User;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="DATETO")
    private LocalDateTime dateTo;
    @Column(name="DATEFROM")
    private LocalDateTime dateFrom;
    @ManyToOne
    @JoinColumn(name="PARKINGTO")
    private Parking parkingTo;
    @ManyToOne
    @JoinColumn(name="PARKINGFROM")
    private Parking parkingFrom;
    @ManyToOne
    @JoinColumn(name="VEHICLE")
    private Vehicle vehicle;
    @ManyToOne
    @JoinColumn(name="USER")
    private User user;

    public long getId() {
        return id;
    }

    public LocalDateTime getDateTo() {
        return dateTo;
    }

    public LocalDateTime getDateFrom() {
        return dateFrom;
    }

    public Parking getParkingTo() {
        return parkingTo;
    }

    public Parking getParkingFrom() {
        return parkingFrom;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public User getUser() {
        return user;
    }

    public Rent() {}

    public Rent(RentDto rent) {
        this.dateTo = rent.getDateTo();
        this.dateFrom = rent.getDateFrom();
        this.parkingTo = new Parking(rent.getParkingTo());
        this.parkingFrom = new Parking(rent.getParkingFrom());
        this.vehicle = new Vehicle(rent.getVehicle());
        this.user = new User(rent.getUser());
    }

    public Rent(LocalDateTime dateTo
            , LocalDateTime dateFrom
            , Parking parkingTo
            , Parking parkingFrom
            , Vehicle vehicle
            , User user) {
        //this.id = id;
        this.dateTo = dateTo;
        this.dateFrom = dateFrom;
        this.parkingTo = parkingTo;
        this.parkingFrom = parkingFrom;
        this.vehicle = vehicle;
        this.user = user;
    }

    public void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo;
    }

    public void setParkingTo(Parking parkingTo) {
        this.parkingTo = parkingTo;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setParkingFrom(Parking parkingFrom) {
        this.parkingFrom = parkingFrom;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
