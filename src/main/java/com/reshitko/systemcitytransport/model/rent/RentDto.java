package com.reshitko.systemcitytransport.model.rent;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.parking.ParkingDto;
import com.reshitko.systemcitytransport.model.user.User;
import com.reshitko.systemcitytransport.model.user.UserDto;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleDto;

import java.time.LocalDateTime;

public class RentDto {
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateTo;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateFrom;
    private ParkingDto parkingTo;
    private ParkingDto parkingFrom;
    private VehicleDto vehicle;
    private UserDto user;

    public RentDto() {}

    public RentDto(Rent rent) {
        this.dateTo = rent.getDateTo();
        this.dateFrom = rent.getDateFrom();
        this.parkingTo = new ParkingDto(rent.getParkingTo());
        this.parkingFrom = new ParkingDto(rent.getParkingFrom());
        this.vehicle = new VehicleDto(rent.getVehicle());
        this.user = new UserDto(rent.getUser());
    }


    public LocalDateTime getDateTo() {
        return dateTo;
    }

    public LocalDateTime getDateFrom() {
        return dateFrom;
    }

    public ParkingDto getParkingTo() {
        return parkingTo;
    }

    public ParkingDto getParkingFrom() {
        return parkingFrom;
    }

    public VehicleDto getVehicle() {
        return vehicle;
    }

    public UserDto getUser() {
        return user;
    }
}
