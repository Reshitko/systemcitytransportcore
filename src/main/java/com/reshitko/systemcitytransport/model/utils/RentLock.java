package com.reshitko.systemcitytransport.model.utils;

import org.hibernate.type.StringNVarcharType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RentLock {
    @Id
    @Column(name="vehicle_serial")
    private String vehicleSerial;

    public RentLock() {}
    public RentLock(String s) {
        this.vehicleSerial = s;
    }
}
