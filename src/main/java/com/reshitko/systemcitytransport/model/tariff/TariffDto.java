package com.reshitko.systemcitytransport.model.tariff;

import java.math.BigDecimal;

public class TariffDto {
    private BigDecimal tariffRubPerMin;
    private String name;

    public TariffDto(Tariff tariff) {
        this.tariffRubPerMin = tariff.getTariffRubPerMin();
        this.name = tariff.getName();
    }
}
