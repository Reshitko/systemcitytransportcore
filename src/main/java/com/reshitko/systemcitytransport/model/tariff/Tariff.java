package com.reshitko.systemcitytransport.model.tariff;

import com.reshitko.systemcitytransport.model.user.UserType;
import com.reshitko.systemcitytransport.model.vehicle.VehicleType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Tariff {
    private UserType userType;
    private VehicleType vehicleType;
    private LocalDateTime dateStart;
    private BigDecimal tariffRubPerMin;
    private String name;

    public Tariff(UserType userType
            , VehicleType vehicleType
            , LocalDateTime dateStart
            , BigDecimal tariffRubPerMin
            , String name) {
        this.userType = userType;
        this.vehicleType = vehicleType;
        this.dateStart = dateStart;
        this.tariffRubPerMin = tariffRubPerMin;
        this.name = name;
    }

    public UserType getUserType() {
        return userType;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public BigDecimal getTariffRubPerMin() {
        return tariffRubPerMin;
    }

    public String getName() {
        return name;
    }
}
