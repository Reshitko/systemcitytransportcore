package com.reshitko.systemcitytransport.model.user;

public enum UserType {
    ADMIN,
    USER
}
