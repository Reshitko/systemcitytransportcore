package com.reshitko.systemcitytransport.model.user;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class User {
    @Id
    @Column
    private String login;
    @Column(name="passwdhash")
    private String password;
    @Column(name="type")
    @Enumerated(EnumType.STRING)
    private UserType type;
    @Column
    private BigDecimal funds;

    public User () {}
    public User(UserDto user) {
        this.funds = user.getBalance();
        this.type = user.getRole();
        this.login = user.getLogin();
    }
    public User(String login, String password, UserType type, BigDecimal funds) {
        this.login = login;
        this.password = password;
        this.type = type;
        this.funds = funds;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public UserType getType() {
        return type;
    }

    public BigDecimal getFunds() {
        return funds;
    }
}
