package com.reshitko.systemcitytransport.model.user;

import java.math.BigDecimal;

public class UserDto {
    private String login;
    private UserType role;
    private BigDecimal balance;

    public UserDto() {}
    public UserDto(User user) {
        this.login = user.getLogin();
        this.role = user.getType();
        this.balance = user.getFunds();
    }

    public String getLogin() {
        return login;
    }

    public UserType getRole() {
        return role;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
