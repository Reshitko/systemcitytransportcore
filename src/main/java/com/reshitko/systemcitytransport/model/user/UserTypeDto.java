package com.reshitko.systemcitytransport.model.user;

public enum UserTypeDto {
    ADMIN,
    USER
}
