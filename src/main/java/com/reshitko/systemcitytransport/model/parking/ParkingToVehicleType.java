package com.reshitko.systemcitytransport.model.parking;

import com.reshitko.systemcitytransport.model.vehicle.VehicleType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@IdClass(ParkingToVehicleTypeId.class)
public class ParkingToVehicleType {
    @Id
    @Column(name="parking_id")
    //@ManyToOne
    //@JoinColumn(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long parkingId;
    @Id
    @Enumerated(EnumType.STRING)
    @Column(name="vehicle_type")
    private VehicleType vehicleType;

    public ParkingToVehicleType() {}
}

