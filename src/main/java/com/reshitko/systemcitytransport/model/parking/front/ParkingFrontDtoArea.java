package com.reshitko.systemcitytransport.model.parking.front;

import java.util.List;

public class ParkingFrontDtoArea {
    private double radiusInMeters;
    private Center center;

    public ParkingFrontDtoArea(double radiusInMeters, Center center) {
        this.radiusInMeters = radiusInMeters;
        this.center = center;
    }

    public double getRadiusInMeters() {
        return radiusInMeters;
    }

    public void setRadiusInMeters(double radiusInMeters) {
        this.radiusInMeters = radiusInMeters;
    }

    public Center getCenter() {
        return center;
    }

    public void setCenter(Center center) {
        this.center = center;
    }
}
