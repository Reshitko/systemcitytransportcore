package com.reshitko.systemcitytransport.model.parking;

import com.reshitko.systemcitytransport.model.vehicle.VehicleType;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class ParkingToVehicleTypeId implements Serializable {
    private long parkingId;
    private VehicleType vehicleType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingToVehicleTypeId that = (ParkingToVehicleTypeId) o;
        return parkingId == that.parkingId && vehicleType == that.vehicleType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingId, vehicleType);
    }
}
