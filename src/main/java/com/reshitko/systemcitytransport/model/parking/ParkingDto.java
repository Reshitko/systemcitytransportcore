package com.reshitko.systemcitytransport.model.parking;

import com.reshitko.systemcitytransport.model.vehicle.VehicleType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ParkingDto {
    private long id;
    private double radius;
    private double lattitude;
    private double longtitude;
    private List<VehicleType> vehicleTypes;
    //private List<ParkingToVehicleType> vehicleTypes;

    public ParkingDto () {}

    public ParkingDto(Parking parking) {
        this.radius = parking.getRadius();
        this.lattitude = parking.getLattitude();
        this.longtitude = parking.getLongtitude();
        this.id = parking.getId();
        this.vehicleTypes = new ArrayList<>(parking.getVehicleTypes());
    }

    public long getId() {
        return id;
    }

    public double getRadius() {
        return radius;
    }

    public double getLattitude() {
        return lattitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public List<VehicleType> getVehicleTypes() {
        return vehicleTypes;
    }
}
