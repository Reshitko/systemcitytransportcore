package com.reshitko.systemcitytransport.model.parking;

import com.reshitko.systemcitytransport.model.parking.front.ParkingFrontDto;
import com.reshitko.systemcitytransport.model.vehicle.VehicleType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Parking {
    @Id
    //@Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private double radius;
    @Column
    private double lattitude;
    @Column
    private double longtitude;
    //@OneToMany
    //@JoinColumn(name="parking_id")
    //@JoinTable(name="parking_to_vehicle_type"
    //        , joinColumns = {@JoinColumn(name = "parking_id")})//,@JoinColumn(name = "vehicle_type")}
            //, inverseJoinColumns = {@JoinColumn(name = "id")})
    @ElementCollection(targetClass = VehicleType.class, fetch = FetchType.LAZY)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="parking_to_vehicle_type")
    @Column(name="vehicle_type") // Column name in person_interest
    private List<VehicleType> vehicleTypes;
    //private List<ParkingToVehicleType> vehicleTypes;

    public Parking() {}
    public Parking (ParkingFrontDto dto) {
        this.id = dto.getId()!=null ? Long.parseLong(dto.getId()) : null;
        this.radius = dto.getArea().getRadiusInMeters();
        this.lattitude = dto.getArea().getCenter().getLatitude();
        this.longtitude = dto.getArea().getCenter().getLongitude();
        //TODO add working statuses
        this.vehicleTypes = new ArrayList<>();
        this.vehicleTypes.add(VehicleType.BYCYCLE);
        this.vehicleTypes.add(VehicleType.SCOOTER);
    }

    public Parking(ParkingDto dto) {
        this.id = dto.getId();
        this.radius = dto.getRadius();
        this.lattitude = dto.getLattitude();
        this.longtitude = dto.getLongtitude();
        this.vehicleTypes = new ArrayList<>(dto.getVehicleTypes());
    }

    public Parking(long id, double radius, double lattitude, double longtitude) {
        this.id = id;
        this.radius = radius;
        this.lattitude = lattitude;
        this.longtitude = longtitude;
    }

    public Parking(double radius, double lattitude, double longtitude) {
        this.radius = radius;
        this.lattitude = lattitude;
        this.longtitude = longtitude;
    }

    public Long getId() {
        return id;
    }

    public double getRadius() {
        return radius;
    }

    public double getLattitude() {
        return lattitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public List<VehicleType> getVehicleTypes() {
        return vehicleTypes;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public void setVehicleTypes(List<VehicleType> vehicleTypes) {
        this.vehicleTypes = vehicleTypes;
    }
}
