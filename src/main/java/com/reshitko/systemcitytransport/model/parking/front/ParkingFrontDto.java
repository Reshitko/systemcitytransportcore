package com.reshitko.systemcitytransport.model.parking.front;

import com.reshitko.systemcitytransport.model.parking.Parking;

import java.util.ArrayList;
import java.util.List;

public class ParkingFrontDto {
    private String id;
    private String name;
    private String type;
    private String status;
    private ParkingFrontDtoArea area;

    public ParkingFrontDto() {}
    public ParkingFrontDto(Parking parking) {
        this.id = parking.getId().toString();
        this.name = String.format("Parking number %s",parking.getId());
        this.type = "ALL";
        this.status = "ACTIVE";
        var tmp = new Center();
        tmp.setLatitude(parking.getLattitude());
        tmp.setLongitude(parking.getLongtitude());
        this.area = new ParkingFrontDtoArea(parking.getRadius(), tmp);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



    public ParkingFrontDtoArea getArea() {
        return area;
    }

    public void setArea(ParkingFrontDtoArea area) {
        this.area = area;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
