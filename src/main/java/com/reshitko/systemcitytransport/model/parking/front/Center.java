package com.reshitko.systemcitytransport.model.parking.front;

public class Center {
    private double latitude;
    private double longitude;

    public Center() {
    }
    public Center(double lat, double lon) {
        this.latitude = lat;
        this.longitude = lon;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
