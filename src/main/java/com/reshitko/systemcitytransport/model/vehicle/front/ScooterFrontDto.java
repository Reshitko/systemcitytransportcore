package com.reshitko.systemcitytransport.model.vehicle.front;

import com.reshitko.systemcitytransport.model.vehicle.Scooter;

public class ScooterFrontDto extends VehicleFrontDto {
    private double maxKmPerHourSpeed;
    private double chargePercent;

    public ScooterFrontDto() {};
    public ScooterFrontDto(Scooter scooter) {
        super(scooter);
        this.chargePercent = scooter.getBatteryChargePercent();
        this.maxKmPerHourSpeed = 0;
    }


    public double getMaxKmPerHourSpeed() {
        return maxKmPerHourSpeed;
    }

    public void setMaxKmPerHourSpeed(double maxKmPerHourSpeed) {
        this.maxKmPerHourSpeed = maxKmPerHourSpeed;
    }

    public double getChargePercent() {
        return chargePercent;
    }

    public void setChargePercent(double chargePercent) {
        this.chargePercent = chargePercent;
    }
}
