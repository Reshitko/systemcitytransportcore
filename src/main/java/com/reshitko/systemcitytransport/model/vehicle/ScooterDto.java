package com.reshitko.systemcitytransport.model.vehicle;

public class ScooterDto extends VehicleDto {
    private double batteryChargePercent;

    public ScooterDto() {}
    public ScooterDto(Scooter scooter) {
        super(scooter);
        this.batteryChargePercent = scooter.getBatteryChargePercent();
    }

    public double getBatteryChargePercent() {
        return batteryChargePercent;
    }

    public void setBatteryChargePercent(double batteryChargePercent) {
        this.batteryChargePercent = batteryChargePercent;
    }
}
