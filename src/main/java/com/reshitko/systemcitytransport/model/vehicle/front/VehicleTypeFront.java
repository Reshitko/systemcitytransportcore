package com.reshitko.systemcitytransport.model.vehicle.front;

public enum VehicleTypeFront {
    BICYCLE(Values.BICYCLE),
    ELECTRIC_SCOOTER(Values.ELECTRIC_SCOOTER);

    private String value;

    VehicleTypeFront(String strRepresentation) {
        this.value = strRepresentation;
    }
    //https://stackoverflow.com/questions/3639225/single-table-inheritance-strategy-using-enums-as-discriminator-value
    public static class Values {
        public static final String BICYCLE = "BICYCLE";
        public static final String ELECTRIC_SCOOTER = "ELECTRIC_SCOOTER";
    }
}
