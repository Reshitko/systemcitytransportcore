package com.reshitko.systemcitytransport.model.vehicle;

public class BycycleDto extends VehicleDto {
    public BycycleDto() {}

    public BycycleDto(Bycycle vehicle) {
        super(vehicle);
    }
}
