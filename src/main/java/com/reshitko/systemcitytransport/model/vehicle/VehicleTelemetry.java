package com.reshitko.systemcitytransport.model.vehicle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class VehicleTelemetry {
    @Id
    @Column(name = "vehicle_id")
    private String serialNumber;
    @Column
    private LocalDateTime datetime;
    @Column(name = "lattitude")
    private double latitude;
    @Column(name = "longtitude")
    private double longitude;

    public VehicleTelemetry() {
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(LocalDateTime datetime) {
        this.datetime = datetime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
