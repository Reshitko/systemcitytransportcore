package com.reshitko.systemcitytransport.model.vehicle;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BycycleDto.class, name = "BYCYCLE"),
        @JsonSubTypes.Type(value = ScooterDto.class, name = "SCOOTER")
})
public class VehicleDto {
    @Enumerated(EnumType.STRING)
    private VehicleType type;
    private VehicleCondition condition;
    private String serialNumber;

    public VehicleDto() {}
    public VehicleDto(Vehicle vehicle) {
        this.serialNumber = vehicle.getSerialNumber();
        this.type = vehicle.getType();
        this.condition = vehicle.getCondition();
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public VehicleCondition getCondition() {
        return condition;
    }

    public void setCondition(VehicleCondition condition) {
        this.condition = condition;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
