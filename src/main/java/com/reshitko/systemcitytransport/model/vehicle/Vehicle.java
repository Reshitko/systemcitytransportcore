package com.reshitko.systemcitytransport.model.vehicle;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.reshitko.systemcitytransport.model.vehicle.front.VehicleFrontDto;
import com.reshitko.systemcitytransport.model.vehicle.front.VehicleTypeFront;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

//https://stackoverflow.com/questions/3915026/how-to-mix-inheritance-strategies-with-jpa-annotations-and-hibernate
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "null")

public class Vehicle {
    @Id
    @Column(name="serial_number")
    private String serialNumber;
    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private VehicleType type;
    @Enumerated(EnumType.STRING)
    private VehicleCondition condition;
    //@Transient
    @Formula("(SELECT r1.dateto is null " +// as isOccupied
            "from RENT r1 join " +
            "(SELECT r0.vehicle, max(r0.datefrom) as _max_date from RENT r0 group by r0.vehicle) " +
            "on r1.datefrom = _max_date where r1.vehicle=serial_number)")
//    @Formula("(SELECT 1=1 from DUAL)")
    private boolean isOccupied;

    public Vehicle() {};
    public Vehicle(String serialNumber, VehicleType type, VehicleCondition condition) {
        this.serialNumber = serialNumber;
        this.type = type;
        this.condition = condition;
    }

    public Vehicle (VehicleFrontDto dto)
    {
        this.serialNumber = dto.getRegNumber();
        this.type = dto.getType() == VehicleTypeFront.BICYCLE ? VehicleType.BYCYCLE : VehicleType.SCOOTER;
        this.condition = VehicleCondition.PERFECT;
    }
    public Vehicle(VehicleDto vehicleDto) {
        this.serialNumber = vehicleDto.getSerialNumber();
        this.condition = vehicleDto.getCondition();
        this.type = vehicleDto.getType();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public VehicleType getType() {
        return type;
    }

    public VehicleCondition getCondition() {
        return condition;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public void setCondition(VehicleCondition condition) {
        this.condition = condition;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }
}
