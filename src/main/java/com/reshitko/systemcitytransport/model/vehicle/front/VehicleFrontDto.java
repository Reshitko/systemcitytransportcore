package com.reshitko.systemcitytransport.model.vehicle.front;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.reshitko.systemcitytransport.access.TripRepository;
import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.parking.front.Center;
import com.reshitko.systemcitytransport.model.parking.front.ParkingFrontDto;
import com.reshitko.systemcitytransport.model.parking.front.ParkingFrontDtoArea;
import com.reshitko.systemcitytransport.model.vehicle.BycycleDto;
import com.reshitko.systemcitytransport.model.vehicle.ScooterDto;
import com.reshitko.systemcitytransport.model.vehicle.Vehicle;
import com.reshitko.systemcitytransport.model.vehicle.VehicleType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.ArrayList;


@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BycycleFrontDto.class, name = "BICYCLE"),
        @JsonSubTypes.Type(value = ScooterFrontDto.class, name = "ELECTRIC_SCOOTER")
})
public class VehicleFrontDto {
    private String status;
    @Enumerated(EnumType.STRING)
    private VehicleTypeFront type;
    private String regNumber;
    private ParkingFrontDto parking;
    private VehicleFrontGeoPosition geoPosition;

    public VehicleFrontDto(Vehicle vehicle, Parking p) {
        this.status = vehicle.isOccupied() ? "OCCUPIED" : "FREE";
        this.type = vehicle.getType() == VehicleType.BYCYCLE
                ? VehicleTypeFront.BICYCLE : VehicleTypeFront.ELECTRIC_SCOOTER;
        this.regNumber = vehicle.getSerialNumber();
        this.parking = new ParkingFrontDto(p);
//        this.parking.setName("TEST PARKING");
//        this.parking.setType("ALL");
//        this.parking.setStatus("ACTIVE");
//        this.parking.setArea(new ParkingFrontDtoArea(100., new Center(56.8515, 35.8838)));
        this.geoPosition = new VehicleFrontGeoPosition(
                p.getLattitude()+Math.random()*0.001, p.getLongtitude()+Math.random()*0.001);
    }

    public VehicleFrontDto(Vehicle vehicle) {
        this.status = vehicle.isOccupied() ? "OCCUPIED" : "FREE";
        this.type = vehicle.getType() == VehicleType.BYCYCLE
                ? VehicleTypeFront.BICYCLE : VehicleTypeFront.ELECTRIC_SCOOTER;
        this.regNumber = vehicle.getSerialNumber();
        this.parking = null;
        this.geoPosition = new VehicleFrontGeoPosition(56.8515, 35.8838);
    }

    public VehicleFrontDto() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public VehicleTypeFront getType() {
        return type;
    }

    public void setType(VehicleTypeFront type) {
        this.type = type;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public ParkingFrontDto getParking() {
        return parking;
    }

    public void setParking(ParkingFrontDto parking) {
        this.parking = parking;
    }

    public VehicleFrontGeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(VehicleFrontGeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }
}
