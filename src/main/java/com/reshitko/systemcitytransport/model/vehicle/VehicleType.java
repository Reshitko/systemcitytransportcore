package com.reshitko.systemcitytransport.model.vehicle;

public enum VehicleType {
    BYCYCLE(Values.BYCYCLE),
    SCOOTER(Values.SCOOTER);
    private String value;

    VehicleType(String strRepresentation) {
        this.value = strRepresentation;
    }
    //https://stackoverflow.com/questions/3639225/single-table-inheritance-strategy-using-enums-as-discriminator-value
    public static class Values {
        public static final String BYCYCLE = "BYCYCLE";
        public static final String SCOOTER = "SCOOTER";
    }
}
