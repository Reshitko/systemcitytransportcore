package com.reshitko.systemcitytransport.model.vehicle.front;

import com.reshitko.systemcitytransport.model.parking.Parking;
import com.reshitko.systemcitytransport.model.vehicle.Bycycle;

public class BycycleFrontDto extends VehicleFrontDto {
    public BycycleFrontDto (Bycycle bycycle, Parking p) {
        super(bycycle, p);
    }
    public BycycleFrontDto (Bycycle bycycle) {
        super(bycycle);
    }
    public BycycleFrontDto() {
    }
}
