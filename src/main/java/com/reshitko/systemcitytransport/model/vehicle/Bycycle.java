package com.reshitko.systemcitytransport.model.vehicle;

import com.reshitko.systemcitytransport.model.vehicle.front.BycycleFrontDto;

import javax.persistence.*;

@Entity
//@Table(name = "vehicle")
@DiscriminatorValue(value = VehicleType.Values.BYCYCLE)
public class Bycycle extends Vehicle {
    public Bycycle() {}
    public Bycycle(BycycleDto bycycleDto) {
        super(bycycleDto);
    }

    public Bycycle(BycycleFrontDto dto) {
        super(dto);
        this.setType(VehicleType.BYCYCLE);
    }
}
