package com.reshitko.systemcitytransport.model.vehicle;

import com.reshitko.systemcitytransport.model.vehicle.front.ScooterFrontDto;

import javax.persistence.*;

@Entity
//@Table(name="Vehicle")
@SecondaryTable(name = "Scooter", pkJoinColumns = @PrimaryKeyJoinColumn(name="serial_number"))
@DiscriminatorValue(value = VehicleType.Values.SCOOTER)
public class Scooter extends Vehicle {
    @Basic(optional = false)
    @Column(table = "Scooter", name = "BATTERY_PERCENTAGE")
    private double batteryChargePercent;

    public Scooter() {};
    public Scooter(ScooterDto scooterDto) {
        super(scooterDto);
        this.batteryChargePercent = scooterDto.getBatteryChargePercent();
    }
    public Scooter(String serialNumber,
                   VehicleType type,
                   VehicleCondition condition,
                   double batteryChargePercent) {
        super(serialNumber, type, condition);
        this.batteryChargePercent = batteryChargePercent;
    }

    public Scooter(ScooterFrontDto dto) {
        super(dto);
        this.batteryChargePercent = dto.getChargePercent();
        this.setType(VehicleType.SCOOTER);
        this.setBatteryChargePercent(100);
    }

    public double getBatteryChargePercent() {
        return batteryChargePercent;
    }

    public void setBatteryChargePercent(double batteryChargePercent) {
        this.batteryChargePercent = batteryChargePercent;
    }
}
