package com.reshitko.systemcitytransport.model.vehicle.front;

public class VehicleFrontGeoPosition {
    private double latitude;
    private double longitude;

    public VehicleFrontGeoPosition(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
