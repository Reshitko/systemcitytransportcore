package com.reshitko.systemcitytransport.model.vehicle;

public enum VehicleCondition {
    PERFECT,
    GOOD,
    FAIR,
    OUT_OF_ORDER
}
